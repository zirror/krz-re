import crypto from "crypto";
import fs from "fs/promises";
import template, { encryptedTemplate } from "./templates.js";
import readpassword from "./cli/readpassword.js";
import passCrypto from "./crypto/index.js";

function createRedirectFile(url, title, _template = template) {
  return {
    content: _template
      .replace(/@@URL@@/g, url)
      .replace(/@@TITLE@@/g, title || "Redirect"),
    url,
    title,
  };
}

async function writeRedirectFile(
  { content, url, title },
  { preHook, postHook, overwrite, getShortFileName, targetFolder, hashLength, urlTitle } = {}
) {
  const id = getShortFileName ? getShortFileName({ url: urlTitle.unencurl || url, hashLength }) : createShortFileName({ url: urlTitle.unencurl || url, hashLength });
  try {
    preHook && preHook({ content, id, url, title, urlTitle });
    await fs.writeFile(targetFolder + id + ".html", content, {
      flag: overwrite ? "w" : "wx",
    });
    postHook && postHook({ content, id, url, title, urlTitle });
  } catch (err) {
    postHook && postHook({ content, id, url, title, urlTitle });
  }
}

function createShortFileName({ url, hashLength }) {
  return crypto
    .createHash("sha256")
    .update(url)
    .digest("base64url")
    .slice(0, hashLength);
}

export default async function GenerateRedirects(
  redirects = [],
  {
    preHook,
    postHook,
    doneHook,
    getShortFileName,
    overwrite,
    targetFolder,
    hashLength = 6,
  } = { hashLength: 6 }
) {
  await fs.mkdir(targetFolder, { recursive: true });
  await Promise.all(
    redirects.map(async (urlTitle) => {
      urlTitle.template = template;
      urlTitle = await transformUrlTitle(urlTitle);
      //always overwrite password protected pages in case password changed
      //unless explicitly set
      let _overwrite = overwrite === undefined ? !!urlTitle.password : overwrite;
      return writeRedirectFile(createRedirectFile(urlTitle.url, urlTitle.title, urlTitle.template), {
        preHook,
        postHook,
        overwrite: _overwrite,
        targetFolder,
        hashLength,
        getShortFileName,
        urlTitle
      })
    }
    )
  );
  doneHook && doneHook();
}

async function transformUrlTitle(urlTitle){
  if(urlTitle.password){
    if(urlTitle.password === true){
      console.log(`Enter password for ${urlTitle.url}`);
      urlTitle.password = await readpassword();
    }
    urlTitle.unencurl = urlTitle.url;
    urlTitle.url = await passCrypto.encrypt(urlTitle.url, urlTitle.password);
    urlTitle.template = encryptedTemplate;
  }
  return urlTitle;
}
