import readline from "readline";



export default async function askForPassword() {
    return new Promise((res) => {

        let rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });

        rl.stdoutMuted = true;

        rl.question('Password: ', function (password) {
            res(password);
            rl.close();
        });

        rl._writeToOutput = function _writeToOutput(stringToWrite) {
            if (rl.stdoutMuted)
                rl.output.write("*");
            else
                rl.output.write(stringToWrite);
        };

    })
}
