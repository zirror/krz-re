//directly from https://www.w3.org/TR/WCAG20-TECHS/H76.html
export default `<!DOCTYPE html>
<html>    
  <head>      
    <title>@@TITLE@@</title>
    <link rel="canonical" href="@@URL@@" />
    <meta name="robots" content="noindex">
    <meta charset="utf-8" />
    <meta http-equiv="refresh" content="0; url='@@URL@@'" />    
  </head>
</html>`;

export const encryptedTemplate =`
<!DOCTYPE html>
<html>    
  <head>      
    <title>Encrypted Redirect</title>
    <meta name="robots" content="noindex">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8" />
    <style>
    body {
      font-family: 'Arial', serif;
    }
    </style>
  </head>
  <body>
    <h1>Password-Protected Redirect</h1>
    <form>
        <label style="height:2rem; font-size: 2rem;">
          Password
          <input style="display:block; min-width: 20ch; width: 20vw; max-width: 80vw; height: 2rem; font-size: 2rem;" type="password">
        </label>
        <button style="margin-top: 2rem; height: 44px; font-size: 2rem">
          Submit
        </button>
    </form>
  </body>
  <script>
    const encryptedUrl="@@URL@@";
    const decryptMessage=function(){"use strict";var e=new Promise((e=>{"undefined"==typeof window?import("crypto").then((t=>e(t.webcrypto))):e(window.crypto)}));const t="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",r="undefined"==typeof Uint8Array?[]:new Uint8Array(256);for(let e=0;e<t.length;e++)r[t.charCodeAt(e)]=e;return async function(t,n,{failWithoutError:a=!1}={}){let i=await e,c=(e=>{let t,n,a,i,c,o=.75*e.length,d=e.length,s=0;"="===e[e.length-1]&&(o--,"="===e[e.length-2]&&o--);const y=new ArrayBuffer(o),l=new Uint8Array(y);for(t=0;t<d;t+=4)n=r[e.charCodeAt(t)],a=r[e.charCodeAt(t+1)],i=r[e.charCodeAt(t+2)],c=r[e.charCodeAt(t+3)],l[s++]=n<<2|a>>4,l[s++]=(15&a)<<4|i>>2,l[s++]=(3&i)<<6|63&c;return y})(t),o=c.slice(0,12),d=c.slice(12,28),s=c.slice(28),y=await async function(t,r){return(await e).subtle.deriveKey({name:"PBKDF2",salt:r,iterations:1e5,hash:"SHA-256"},t,{name:"AES-GCM",length:256},!0,["encrypt","decrypt"])}(await async function(t){let r=await e,n=new TextEncoder;return r.subtle.importKey("raw",n.encode(t),{name:"PBKDF2"},!1,["deriveBits","deriveKey"])}(n),d);try{let e=await i.subtle.decrypt({name:"AES-GCM",iv:o},y,s);return(new TextDecoder).decode(e)}catch(e){if(a)return;throw e}}}();
    const form = document.querySelector('form');
    form.addEventListener("submit", (e) => {
      e.preventDefault();
      const passw = document.querySelector('input[type="password"]');
      decryptMessage(encryptedUrl, passw.value).then(x => {
        window.location = x;
      }).catch(x => {
        window.alert("Wrong password!");
      });
      return false;
    })
  </script>
</html>`;