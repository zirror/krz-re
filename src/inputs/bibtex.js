//You can use this to """""""Parse"""""""" Bibtex in order to replace all URLs with shortend URLs

const URL_REGEX = new RegExp(/url = {([^}]+)}/g);

export default function readBibtex(bibtex) {
    const result = [];
    let matchedUrls;
    do {
        matchedUrls = URL_REGEX.exec(bibtex);
        if (matchedUrls) {
            result.push({
                title: "Redirect",
                url: matchedUrls[1]
            });
        }
    } while (matchedUrls);
    return result;
}