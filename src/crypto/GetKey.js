import _crypto from "./crypto.js";

export async function GetKey(keyMaterial, salt) {
  let crypto = await _crypto;
  return crypto.subtle.deriveKey(
    {
      "name": "PBKDF2",
      salt: salt, 
      "iterations": 100000,
      "hash": "SHA-256"
    },
    keyMaterial,
    { "name": "AES-GCM", "length": 256},
    true,
    [ "encrypt", "decrypt" ]
  );
}


export async function GetKeyMaterial(password) {
  let crypto = await _crypto;
  let enc = new TextEncoder();
  return crypto.subtle.importKey(
    "raw", 
    enc.encode(password), 
    {name: "PBKDF2"}, 
    false, 
    ["deriveBits", "deriveKey"]
  );
}
