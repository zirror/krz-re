import {GetKey, GetKeyMaterial} from "./GetKey.js";
import {encode} from "../../libs/base64conversion.js";
import _crypto from "./crypto.js";

function encodeMessage(message){
    let enc = new TextEncoder();
    let result = enc.encode(message);
    return result;
}

export default async function encryptMessage(message, password) {
    let crypto = await _crypto;
    let encoded = encodeMessage(message);
    let iv = crypto.getRandomValues(new Uint8Array(12));
    let salt = crypto.getRandomValues(new Uint8Array(16));
    let key = await GetKey(await GetKeyMaterial(password), salt);
    let ciphertext = await crypto.subtle.encrypt(
      {
        name: "AES-GCM",
        iv: iv
      },
      key,
      encoded
    );
    
    let buffer = new Uint8Array(ciphertext);
    //first 12 bytes are the initialization vector
    //second 16 bytes are salt

    let concat =  new Uint8Array(iv.length + salt.length + buffer.length);
    concat.set(iv);
    concat.set(salt, iv.length);
    concat.set(buffer, iv.length + salt.length);
    let ivAndCt = `${encode(concat)}`;
    return ivAndCt;
}
