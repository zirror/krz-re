import encrypt from "./AESEncrypt.js";
import decrypt from "./AESDecrypt.js";

export default {
    encrypt,
    decrypt
}