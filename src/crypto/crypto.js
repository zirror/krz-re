
export default new Promise((res) => {
    /*eslint-disable*/
    if(typeof window == "undefined"){
      import("crypto").then(v => res(v.webcrypto));
    }else{
      res(window.crypto);
    }
})