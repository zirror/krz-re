import { GetKey, GetKeyMaterial } from "./GetKey.js";
import {decode} from "../../libs/base64conversion.js";
import _crypto from "./crypto.js";


export default async function decryptMessage(base64encrypted, password, {failWithoutError = false} = {}) {
  let crypto = await _crypto;
  //iv is the first 12 bytes, salt the second 16 bytes
  let ivsaltandct = decode(base64encrypted);
  let iv = ivsaltandct.slice(0, 12);
  let salt = ivsaltandct.slice(12, 12 + 16);
  let ciphertext = ivsaltandct.slice(12 + 16);
  let key = await GetKey(await GetKeyMaterial(password), salt);
  try {
    let decrypted = await crypto.subtle.decrypt(
      {
        name: "AES-GCM",
        iv: iv
      },
      key,
      ciphertext
    );
    let dec = new TextDecoder();
    return dec.decode(decrypted);
  } catch (e) {
    if (failWithoutError) {
      return undefined;
    }
    throw e;
  }
}
