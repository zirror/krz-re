import createRedirects from "./src/genUri.js";
import fs from "fs";

const generatedurls = [];
let source = JSON.parse(
  fs.readFileSync("./config.json", {
    encoding: "utf-8",
  })
);
const domain = source.baseurl || "https://example.com/";
const targetFolder = source.targetFolder || "./public/";

createRedirects(source.links, {
  targetFolder,
  postHook: (args) => {
    generatedurls.push({
      orig: args.urlTitle.unencurl || args.urlTitle.url,
      shortend: domain + args.id,
    });
  },
  doneHook: () => {
    console.log("This is your list of generated urls");
    console.log(generatedurls);
  },
});
