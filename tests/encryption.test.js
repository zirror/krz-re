import decrypt from "../src/crypto/AESDecrypt.js";
import encrypt from "../src/crypto/AESEncrypt.js";
import test from "ava";

let PasswordCrypto = {
    decryptMessage: decrypt,
    encryptMessage: encrypt
}


test('encrypted-decrypted', async t => {
  t.is(await PasswordCrypto.decryptMessage(await PasswordCrypto.encryptMessage("encrypt me", "1234"), "1234"), "encrypt me");
})

test('encrypted-decrypted url', async t => {
    const testurl  = "Kn9028TQ7OOYELpeC1qQ5re3OZ3oAvdGDDMYAhpI7xR6Tr5Ihz5OqxGTn92DSYLxCqROu7fnasFKGBp2cUd4AUDnd5NfFy/I0I37YfV31w==";
    t.is(await PasswordCrypto.decryptMessage(testurl, "password1234"), "https://mysecreturl.com/very-secret");
})

test('encrypted-decrypted console password', async t => {
    const testurl  = "NZpURNvMYYt2LOMDx2hSKyMbn/cMMic38gChwf9XCCHPzdXO8UX4geilXbrWr19PXBzta9yKVS5UnJnwgyJj1DKfXpyDk0Rfk7wY8V+VQ4AbOrqj";
    t.is(await PasswordCrypto.decryptMessage(testurl, "test1234"), "https://myothersecreturl.com/very-secret");
})
  