# Krz.Re

Krz.Re is a simple static URL-shortener. This is only for in-browser use. If you need high-performance redirecting, you should use an actual server.
If you're a citizen of the EU, I can recommend the '.re' TLD for your own shortener. 

## Usage

1. Edit `config.json` to your liking.
2. ~~`yarn install`~~ actually, you don't need that, since this has 0 dependencies!
3. `yarn generate`
4. Push it to a gitlab repo and publish it using [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/)

You could also generate the pages only during Gitlab CI.

## Development

The rollup config is only for encrypted templates.

## Thanks

- Gitlab for having Gitlab Pages
- https://github.com/brunoluiz/urlzap for the inspiration
