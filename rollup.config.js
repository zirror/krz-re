import { terser } from "rollup-plugin-terser";
export default {
    input: "src/crypto/AESDecrypt.js",
    output: {
        format: "iife",
        name: "decryptMessage"
    },
    plugins: [terser()]
}
